// create routes
const express = require('express')

// import to use .Router() method
// because .Router() is an express method
// Router() handles the requests
const router = express.Router()

// User Controller to use invoke()
const userController = require('../controllers/userController')
const auth = require('../auth')

// 🆘⚠️ Never re-arrange the order of the routes!!! ⚠️🆘
// 1) getAllUsers => '/'
router.get('/', (req, res) => {
  userController.getAllUsers().then((result) => res.send(result))
})

// 2) getUserProfile => '/details'
router.get('/details', auth.verify, (req, res) => {
  // console.log(req.headers.authorization);
  let userData = decode(req.headers.authorization)
  userController.getUserProfile(userData).then((result) => res.send(result))
})

// 3) registerUser => '/register'
router.post('/register', (req, res) => {
  //expecting data/object coming from request body
  userController.registerUser(req.body).then((result) => res.send(result))
})

// 4) login => '/login'
router.post('/login', (req, res) => {
  userController.login(req.body).then((result) => res.send(result))
})

// 5) checkEmail => '/email-exists'
router.post('/email-exists', (req, res) => {
  userController.checkEmail(req.body.email).then((result) => res.send(result))
})

// 6) editUser => '/edit'
router.put('/edit', auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id
  userController.editUser(userId, req.body).then((result) => res.send(result))
})

// 7) editDetails => '/edit-profile'
router.put('/edit-profile', (req, res) => {
  console.log(req.body)
  userController.editDetails(req.body).then((result) => res.send(result))
})

// 8) delete => '/:userId/delete'
router.delete('/:userId/delete', (req, res) => {
  // console.log(req.params.userId)
  return userController
    .delete(req.params.userId)
    .then((result) => res.send(result))
})

// 9) editProfile => '/:userId/edit'
router.put('/:userId/edit', (req, res) => {
  const userId = req.params.userId

  userController
    .editProfile(userId, req.body)
    .then((result) => res.send(result))
})

// 10) deleteUser => '/delete'
router.delete('/delete', (req, res) => {
  return userController
    .deleteUser(req.body.email)
    .then((result) => res.send(result))
})

// 901
router.post('/enroll', auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId
  }
  userController.enroll(data).then((result) => res.send(result))
})

/*
router.post('/new-enroll', auth.verify, (req, res) => {
  let data 
})
*/
/*
 
//create a course
router.post("/create-course", auth.verify, (req, res) => {
	courseController.createCourse(req.body).then(result => res.send(result))
})


//retrieving all courses
router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})


//retrieving only active courses
router.get("/active-courses", auth.verify, (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

//get a specific course using findOne()
router.get("/specific-course", auth.verify, (req, res) => {

	// console.log(req.body)	//object

	courseController.getSpecificCourse(req.body.courseName).then( result => res.send(result))
})


//get specific course using findById()
router.get("/:courseId", auth.verify, (req, res) => {

	// console.log(req.params)	//{ courseId: '61979f60f63f4531cd77b395' }
	let paramsId = req.params.courseId
	courseController.getCourseById(paramsId).then(result => res.send(result))
})

//update isActive status of the course using findOneAndUpdate()
	//update isActive status to false
router.put("/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.body.courseName).then( result => res.send(result))
})

	//update isActive status to true
router.put("/unarchive", auth.verify, (req, res) => {

	courseController.unarchiveCourse(req.body.courseName).then( result => res.send(result))
})


	//update isActive status to true
router.put("/:courseId/unarchive", auth.verify, (req, res) => {

	courseController.unarchiveCourseById(req.params.courseId).then( result => res.send(result))
})

//update isActive status of the course using findByIdAndUpdate()
	//update isActive status to false
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourseById(req.params.courseId).then(result => res.send(result))
})




//delete course using findOneAndDelete()
router.delete("/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourse(req.body.courseName).then(result => res.send(result))
})

//delete course using findByIdAndDelete()
router.delete("/:courseId/delete-course", auth.verify, (req, res) => {

	courseController.deleteCourseById(req.params.courseId).then(result => res.send(result))
})


router.put("/:courseId/edit", auth.verify, (req, res) => {

	courseController.editCourse(req.params.courseId, req.body).then( result => res.send(result))
})



module.exports = router;

*/

module.exports = router
