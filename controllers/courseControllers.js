const Course = require('../models/Course')

module.exports.createCourse = (reqBody) => {
  const { courseName, description, price, isActive, createdOn } = reqBody

  const newCourse = new Course({
    courseName: courseName,
    description: description,
    price: price,
    isActive: isActive,
    createdOn: createdOn
  })

  return newCourse.save().then((result, error) => {
    return result ? true : error
  })
}

module.exports.getAllCourses = () => {
  return Course.find().then((result, error) => {
    return error ? error : result
    // false : true (alternative)
  })
}

module.exports.archive = (userId) => {
  return Course.findByIdAndUpdate(userId, { isActive: false }).then(
    (result) => {
      return result ? true : false
    }
  )
}

module.exports.unArchive = (userId) => {
  return Course.findByIdAndUpdate(userId, { isActive: true }).then((result) => {
    return result ? true : false
  })
}
